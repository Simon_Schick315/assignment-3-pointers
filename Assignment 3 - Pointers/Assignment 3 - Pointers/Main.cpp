
// Assignment 3 - Pointers
// Simon Schick


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int *pFirst, int *pSecond)
{
	// Stores a copy of the first pointer variable. Don't need to store a copy of both
	int i = *pFirst;

	// swaps both values
	*pFirst = *pSecond;
	*pSecond = i;
}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
